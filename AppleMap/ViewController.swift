//
//  ViewController.swift
//  AppleMap
//
//  Created by 陳鍵群 on 2019/3/18.
//  Copyright © 2019年 陳鍵群. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    var longtitude = 0.0
    var latitude = 0.0
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(action(gestureRecognizer:))))
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.authorizationStatus() == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        longtitude = locations.first?.coordinate.longitude ?? 0.0
        latitude = locations.first?.coordinate.latitude ?? 0.0
    }

    @objc func setupData() {
        // 1. 檢查系統是否能夠監視 region
        if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            
            // 2.準備 region 會用到的相關屬性
            let title = "Lorrenzillo's"
            let coordinate = CLLocationCoordinate2D.init(latitude: self.latitude, longitude: self.longtitude)
            let regionRadius = 800.0
            
            // 3. 設置 region 的相關屬性
            let region = CLCircularRegion(center: CLLocationCoordinate2D(latitude: coordinate.latitude,
                                                                         longitude: coordinate.longitude), radius: regionRadius, identifier: title)
            locationManager.startMonitoring(for: region)
            
            // 4. 創建大頭釘(annotation)
            let restaurantAnnotation = MKPointAnnotation()
            restaurantAnnotation.coordinate = coordinate;
            restaurantAnnotation.title = "\(title)";
            mapView.addAnnotation(restaurantAnnotation)
            
            let aAnnotation = MKPointAnnotation()
            aAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude + 0.00025, longitude: coordinate.longitude + 0.00025)
            aAnnotation.title = "123";
            mapView.addAnnotation(aAnnotation)
            
            let bAnnotation = MKPointAnnotation()
            bAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude + 0.00125, longitude: coordinate.longitude + 0.00125)
            bAnnotation.title = "123";
            mapView.addAnnotation(bAnnotation)
            
            let cAnnotation = MKPointAnnotation()
            cAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude + 0.0035, longitude: coordinate.longitude - 0.0035)
            cAnnotation.title = "123";
            mapView.addAnnotation(cAnnotation)
            
            let dAnnotation = MKPointAnnotation()
            dAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude - 0.00025, longitude: coordinate.longitude - 0.00025)
            dAnnotation.title = "123";
            mapView.addAnnotation(dAnnotation)
            
            let eAnnotation = MKPointAnnotation()
            eAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude + 0.0015, longitude: coordinate.longitude + 0.0015)
            eAnnotation.title = "123";
            mapView.addAnnotation(eAnnotation)
            
            let fAnnotation = MKPointAnnotation()
            fAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude - 0.00025, longitude: coordinate.longitude - 0.0065)
            fAnnotation.title = "123";
            mapView.addAnnotation(fAnnotation)
            
            let gAnnotation = MKPointAnnotation()
            gAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude + 0.001021155, longitude: coordinate.longitude - 0.0071055)
            gAnnotation.title = "123";
            mapView.addAnnotation(gAnnotation)
            
            let hAnnotation = MKPointAnnotation()
            hAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude + 0.0010975, longitude: coordinate.longitude - 0.006155)
            mapView.addAnnotation(hAnnotation)

            let iAnnotation = MKPointAnnotation()
            iAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude - 0.00101155, longitude: coordinate.longitude + 0.005190355)
            iAnnotation.title = "123";
            mapView.addAnnotation(iAnnotation)
            
            let jAnnotation = MKPointAnnotation()
            jAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude - 0.00101155, longitude: coordinate.longitude + 0.005190355)
            jAnnotation.title = "123";
            mapView.addAnnotation(jAnnotation)

            let kAnnotation = MKPointAnnotation()
            kAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude - 0.00101155, longitude: coordinate.longitude + 0.005190355)
            kAnnotation.title = "123";
            mapView.addAnnotation(kAnnotation)

            let lAnnotation = MKPointAnnotation()
            lAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude - 0.00101155, longitude: coordinate.longitude - 0.006190355)
            lAnnotation.title = "123";
            mapView.addAnnotation(lAnnotation)

            let mAnnotation = MKPointAnnotation()
            mAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude - 0.00101155, longitude: coordinate.longitude + 0.007190355)
            mAnnotation.title = "123";
            mapView.addAnnotation(mAnnotation)

            let nAnnotation = MKPointAnnotation()
            nAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude - 0.00131155, longitude: coordinate.longitude + 0.005190355)
            nAnnotation.title = "123";
            mapView.addAnnotation(nAnnotation)

            let oAnnotation = MKPointAnnotation()
            oAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude - 0.00151155, longitude: coordinate.longitude + 0.005190355)
            oAnnotation.title = "123";
            mapView.addAnnotation(oAnnotation)
            
            let pAnnotation = MKPointAnnotation()
            pAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude - 0.00125, longitude: coordinate.longitude - 0.0065)
            pAnnotation.title = "123";
            mapView.addAnnotation(pAnnotation)
            
            let qAnnotation = MKPointAnnotation()
            qAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude - 0.001325, longitude: coordinate.longitude )
            qAnnotation.title = "123";
            mapView.addAnnotation(qAnnotation)
            
            let rAnnotation = MKPointAnnotation()
            rAnnotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude - 0.00025, longitude: coordinate.longitude)
            rAnnotation.title = "123";
            mapView.addAnnotation(rAnnotation)

           
            
            // 5. 繪製一個圓圈圖形（用於表示 region 的範圍）
            let circle = MKCircle(center: coordinate, radius: regionRadius)
            mapView.addOverlay(circle)
        }
        else {
            print("System can't track regions")
        }
    }
    
    @objc func action(gestureRecognizer:UIGestureRecognizer){
        
        let touchPoint = gestureRecognizer.location(in: mapView)
        let newCoordinates = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        let annotation = MKPointAnnotation()
        annotation.coordinate = newCoordinates
        mapView.addAnnotation(annotation)
        guard count  < 1 else {return}
        setupData()
        count += 1
    }
    
    // 6. 繪製圓圈
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let circleRenderer = MKCircleRenderer(overlay: overlay)
        circleRenderer.strokeColor = UIColor.red
        circleRenderer.lineWidth = 1.0
        return circleRenderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "MyPin"
        
        if annotation.isKind(of: MKUserLocation.self) {
            return nil
        }
        

        
        // Reuse the annotation if possible
        var annotationView:MKPinAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
        }
        
        let leftIconView = UIImageView(frame: CGRect(x: 0, y: 0, width: 53, height: 53))
        annotationView?.leftCalloutAccessoryView = leftIconView
        
        if annotation.title == "123" {
            annotationView?.pinTintColor = UIColor.green
        }
        
        
        return annotationView
    }
    
    
    
}

